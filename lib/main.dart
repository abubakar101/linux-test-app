import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:process_run/process_run.dart';

const Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: darkBlue,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: MyWidget(),
        ),
      ),
    );
  }
}

class MyWidget extends StatefulWidget {
  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  bool display = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () async {
          setState(() {
            display = false;
          });

          /// if you want to change the working directory
          var shell = Shell(workingDirectory: "/Users/mac");
          try {
            await shell.run('''
pwd
python test.py
''');
          } on ShellException catch (e) {
            log("Exception -> ${e.result?.errText}");
          }
          // print(result);
          setState(() {
            display = true;
          });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: !display,
              child: const Center(child: CircularProgressIndicator()),
            ),
            Visibility(
              visible: display,
              child: Center(
                child: Text(
                  'Hello, World!',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
